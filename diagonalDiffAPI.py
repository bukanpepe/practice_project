from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/diagonal', methods=['POST'])
def diagonalDifference():
    req_body = request.get_json()
    arr = req_body['Array']
    n = len(arr)
    left = 0
    right = 0

    for i in range(n):
        left += arr[i][i]
        right += arr[i][n-i-1]

    return {"Left & Right difference" : abs(left - right)}
