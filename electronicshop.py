from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/electronicshop', methods=['GET','POST'])
def getMoneySpent():
    req_body = request.get_json()
    keyboards = req_body['Keyboards']
    drives = req_body['Drives']
    b = req_body['Money']
    
    ans = -1
    keyboards.sort(reverse=True)
    drives.sort()
  
    for i in keyboards:
      for j in drives:
          if i + j > b:
              break
          ans = max(ans, i + j)
    return str(ans)
  