from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/birds', methods = ['POST'])
def migratoryBirds():
    req_body = request.get_json()
    arr = req_body['Array']
    
    test = []
    for i in range(1,6):
    test.append(arr.count(i))

    return {"Type": test.index(max(test))+1}