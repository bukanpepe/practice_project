from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/betweentwosets', methods=['POST'])
def betweenTwo():
    req_body = request.get_json()
    a = req_body['Array A']
    b = req_body['Array B']
    list = []
    
    for i in range(a[-1],(b[0]+1)):
        x = 0
        y = 0
        for j in a:
            if i%j == 0:
                x += 1
        if x == len(a):
            for k in b:
                if k%i == 0:
                    y += 1
        if y == len(b):
            list.append(i)

    return str(list)