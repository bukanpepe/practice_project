from flask import Flask

app = Flask(__name__)
@app.route('/pages/<n>/<p>')

def pageCount(n, p) :
	n = int(n)
	p = int(p)
	pages = []
	comp1 = 0
	comp2 = 0
	for i in range(1,n+1):
		pages.append(i)

	if pages[0] == 0 or p in pages[-2:]:
		return str(0)
    
	comp1 = p//2
	comp2 = (n - p)//2
	if comp1 < comp2:
		return str(comp1)
	else:
		return str(comp2)