from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/<name>', methods=['GET','POST'])
def greetings(name):
	x = 'greetings ' + name
	return x
    
@app.route('/circular', methods=['POST'])
def circularArrayRotation():

    req_body = request.get_json()
    a = req_body['array']
    k = req_body['rotation']
    queries = req_body['queries']
    
    b = []
    res = []
    for i in range(0,k):
      pop = int(a.pop())
      b = a
      b.insert(0,pop)
    for i in queries:
      res.append(b[i])
      
    return {"Result": res}