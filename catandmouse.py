from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/catandmouse', methods=['GET','POST'])
def catAndMouse():
    x = int(request.args.get('Cat_A'))
    y = int(request.args.get('Cat_B'))
    z = int(request.args.get('Mouse_C'))
    
    zx = abs(z - x)
    zy = abs(z - y)

    if zx < zy:
        return "Cat A"
    elif zx > zy:
        return "Cat B"
    elif zx == zy:
        return "Mouse C"