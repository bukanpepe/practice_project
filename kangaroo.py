from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/<name>', methods=['GET','POST'])
def greetings(name):
	x = 'greetings ' + name
	return x

@app.route('/kangaroo', methods=['POST']) 
def kangaroo():
    req_body = request.get_json()
    x1 = req_body['x1']
    v1 = req_body['v1']
    x2 = req_body['x2']
    v2 = req_body['v2']
    
    if v2 == v1:
        return ("NO")
    else:
        for i in range((x1+v1)*(x2+v2)):
            if x1 + (v1*i) == x2 + (v2*i):
                return {"Kangaroo A & B met at ": str(i) + " jumps"}
            else:
                continue
                return ("NO")
    