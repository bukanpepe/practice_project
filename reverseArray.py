from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/<name>', methods=['GET','POST'])
def greetings(name):
	x = 'greetings ' + name
	return x
    
@app.route('/reverseArray', methods=['POST'])
def reverseArray():
    req_body = request.get_json()
    arr = req_body['Array']
    i = 0
    k = -1

    while i < len(arr)//2:
      arr[i],arr[k] = arr[k],arr[i]
      i += 1
      k -= 1
    return str(arr)