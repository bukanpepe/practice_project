SELECT DISTINCT city
FROM station
WHERE (LEFT(city,1) = 'a' OR LEFT(city,1) = 'i' OR LEFT(city,1) = 'u' OR LEFT(city,1) = 'e' OR LEFT(city,1) = 'o') AND (RIGHT(city,1) = 'o' OR RIGHT(city,1) = 'a' OR RIGHT(city,1) = 'e' OR RIGHT(city,1) = 'i' OR RIGHT(city,1) = 'u');