from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/time', methods=['GET','POST'])
def timeInWords():
  numbers = ["zero", "one","two","three","four","five","six","seven","eight","nine","ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen","twenty","twenty one","twenty two","twenty three","twenty four","twenty five","twenty six","twenty seven","twenty eight","twenty nine"]
  h = int(request.args.get('hour'))
  m = int(request.args.get('min'))
  
  if m == 00:
    return numbers[h] + " o' clock"
  elif m == 30:
    return "half past " + numbers[h]
  elif m < 30:
    if m == 1:
      return "one minute past " + numbers[h]
    elif m == 15:
      return "quarter past " + numbers[h]
    else:
      return numbers[m] + " minutes past " + numbers[h]
  elif m > 30:
    if m == 45:
      return "quarter to " + numbers[h+1]
    else:
      return numbers[60-m] + " minutes to " + numbers[h+1]