from flask import Flask

app = Flask(__name__)

@app.route('/time/<hh>/<mm>/<ss>/<amp>')

def time_conversion(hh,mm,ss,amp):

	t= hh + ":" + mm + ":" + ss + amp
	if t[-2:] == "PM" and t[:2] != "12":
		t = t[:-2]
		t = str(int(t[:2]) + 12) + t[2:]
	if t[-2:] == "PM" and t[:2] == "12":
		t = t[:-2]
	if t[-2:] == "AM":
		t = t[:-2]
		if t[:2] == "12":
			t = "00" + t[2:]
 
	return (t)