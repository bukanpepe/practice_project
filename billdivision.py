from flask import Flask
from flask import request
import numpy

app = Flask(__name__)

@app.route('/bill/<skip>', methods=['GET','POST'])
def bonAppetit(skip):
    req_body = request.get_json()
    bill = req_body['Bill']
    k = int(skip)
    charged = int(request.args.get('charged'))
    
    x = bill.pop(k)
    shared = numpy.sum(bill)//2

    if shared == charged:
        return "Bon Appetit"
    else:
        return {"Returned": str(charged - shared)}