from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/hourglass', methods=['POST'])
def hourglass():
    req_body = request.get_json()
    arr = req_body['Array']

    sum = []

    for i in range(len(arr)-2):
      for j in range(len(arr)-2):
        sum.append(arr[i][j]+arr[i][j+1]+arr[i][j+2]+arr[i+1][j+1]+arr[i+2][j]+arr[i+2][j+1]+arr[i+2][j+2])
    return str(max(sum))
