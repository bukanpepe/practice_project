from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/rotateleft/<d>', methods=['GET','POST'])
def rotateLeft(d):
    d = int(d)
    req_body = request.get_json()
    arr = req_body['Array']
    i = 0
    
    while i < d:
        x = arr.pop(0)
        arr.append(x)
        i += 1
    return {"Rotated": str(arr)}
