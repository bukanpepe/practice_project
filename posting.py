from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/', methods=['GET','POST'])
def greetings():
	x = 'greetings'
	return x

@app.route('/linear_search/<search>', methods=['POST'])
def linear_search(search):
	x = int(search)
	req_body = request.get_json()
	arr = req_body['data']
	
	for i in arr:
  		if i == x:
    			return {"found at index ": arr.index(i)}
  		elif i == arr[-1]:
    			return {"search": "not found"}
