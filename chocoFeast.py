from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/choco', methods=['POST'])
def chocoFeast():
    req_body = request.get_json()
    n = req_body['Money']
    c = req_body['Price']
    m = req_body['Exchange']
    
    choco = n//c
    wrap = choco
    rest = 0

    while wrap >= m:
      rest = wrap//m
      print(rest)
      choco += rest
      rest += wrap%m
      print(rest)
      wrap = rest

    return {"Choco": choco}