from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/data', methods=['POST'])
def time_conversion():
	t = request.args.get('time')

	if t[-2:] == "PM" and t[:2] != "12":
		t = t[:-2]
		t = str(int(t[:2]) + 12) + t[2:]
	if t[-2:] == "PM" and t[:2] == "12":
		t = t[:-2]
	if t[-2:] == "AM":
		t = t[:-2]
		if t[:2] == "12":
			t = "00" + t[2:]
	return(t)