from flask import Flask
from flask import request
import basicSort
import mergeSort

app = Flask(__name__)

@app.route('/basic', methods=['POST'])
def basicAlgo():
    req_query = request.args.get('reverse')
    req_body = request.get_json()
    arr = req_body['Array']

    if arr == []:
        return "Please give an array"
    else:
        if req_query == "False":
            return str(basicSort.sortAsc(arr))
                    
        elif req_query == "True":
            return str(basicSort.sortDesc(arr))
        else:
            return "Please fill query parameter"
            
@app.route('/merge', methods=['POST'])
def mergeAlgo():
    req_body = request.get_json()
    myList = req_body['Array']
    
    if myList == []:
        return "Please give an array"
    else:
        return str(mergeSort.mergeSort(myList))

