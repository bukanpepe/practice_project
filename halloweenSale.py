from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/sale', methods=['GET','POST'])
def halloweenSale():
    req_body = request.get_json()
    p = req_body['p']
    d = req_body['d']
    m = req_body['m']
    s = req_body['s']
    
    total = p
    count = 1
    while total < s:
        p -= d
        total += max(p,m)
        count += 1
    
    if total > s:
        count -= 1

    return {"Total games bought" : + count}
