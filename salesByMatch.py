from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/socks', methods=['GET','POST'])
def salesByMatch():
    req_body = request.get_json()
    arr = req_body['Socks']
    
    ava = []
    test = 0
    for i in arr:
        if i not in ava:
            ava.append(i)

    for j in ava:
        test += arr.count(j)//2
        
    return {"Pair of socks": test}