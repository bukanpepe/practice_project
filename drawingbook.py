from flask import Flask
from flask import request

app = Flask(__name__)

@app.route('/<name>', methods=['GET','POST'])
def greetings(name):
	x = 'greetings ' + name
	return x

@app.route('/drawingbook/pages', methods=['POST'])    
def page_count():
    totpage = request.args.get('total')
    n = int(totpage)
    req_body = request.get_json()
    p = req_body['#page']
    if(p//2 < (n-p)//2):
        return ("flip " + str(p//2) + " times from the front")
    else:
        return ("flip " + str((n-p)//2) + " time from the back")
    
    